aur-get is an aptitude-like aur-helper wrote in bash

**Dependencies**
* curl
* git
* makepkg
* grep

**Usage**
For installing a package: ``$ aur-get install foobar``  
For searching a package: ``$ aur-get search foobar``  
